# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the shoppinglist.koenvervloesem package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: shoppinglist.koenvervloesem\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-16 15:31+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:43 shoppinglist.desktop.in.h:1
msgid "Shopping List"
msgstr ""

#: ../qml/Main.qml:44
msgid "Never forget what to buy"
msgstr ""

#: ../qml/Main.qml:58
msgid "Add"
msgstr ""

#: ../qml/Main.qml:70
msgid "Shopping list item"
msgstr ""
