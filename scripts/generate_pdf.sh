#!/bin/bash

# Do we have the necessary executables?
for exe in asciidoctor-pdf ps pdffonts identify convert
do
	which ${exe} > /dev/null || ( echo "$exe not found. Please install first. We need asciidoctor-pdf, ghostscript, poppler-utils, imagemagick" ; exit )
done

bookversion=1.1
bookdate=$(date --rfc-3339=date)
year=$( date +%Y )
month=$( date +%m )
FILE=trainingbook
countrycode="en"

echo "Generate input files from course content"
for file in $( ls trainingpart1module?.adoc )
do
  sed -n '19,$p' $file | head -n -2  > $file.adoc
done
echo "Hacking index.adoc"
cat index.adoc | sed -e 's/link:trainingpart.module..html\[/=== /' | sed -e 's/Title}]/Title}/' > index.adoc.adoc

res1=${SECONDS}
# Default theme: /var/lib/gems/2.7.0/gems/asciidoctor-pdf-1.5.3/data/themes/default-theme.yml
asciidoctor-pdf --trace -v  --doctype book -d book \
-a lang=en \
-a bookdate="$bookdate" \
-a year="$year" \
-a month="$month" \
-a bookversion="$bookversion" \
-a countrycode="$countrycode" \
-a pdf-theme=book-theme.yml \
-a pagenums \
-a checks \
"$FILE.adoc"  1> go.log 2>&1  || exit


echo Checking for existence of images with transparancy:
for dir in  $( ls images )  
do
  for file in  "images/$dir"/*.png
  do 
    echo "$file"
    identify -format '%[channels]'  "$file" | grep a
    ret=$?
    if [[ $ret -eq 0 ]]
    then
      echo "Unwanted transparancy found in $file"
      convert "$file"  -alpha off "$file"
    fi
  done
done

echo
echo "Doing pdf2pdf..."
echo "*******************************************************************************************************************************************"
time gs -q -P- -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sPAPERSIZE=c4 "-sOutputFile=$FILE-prepress.pdf" -dPDFSETTINGS=/prepress  -f $FILE.pdf
echo "*******************************************************************************************************************************************"

pdffonts "$FILE.pdf"




